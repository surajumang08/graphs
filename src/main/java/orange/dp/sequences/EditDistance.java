/*
 * Copyright 2006-2018 (c) Care.com, Inc.
 * 1400 Main Street, Waltham, MA, 02451, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Care.com, Inc. ("Confidential Information").  You shall not disclose
 * such Confidential Information and shall use it only in accordance with
 * the terms of an agreement between you and CZen.
 */
package orange.dp.sequences;

/**
 * Created 3/23/2019
 *
 * @author sjkumar
 */
// write a program to solve the generic edit distance problem.
// [todo] Apply it on Abbreviations and LongestCommonSub-sequence.
public class EditDistance {
}
